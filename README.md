"Git exercises" is a series of exercises on advanced `git` topics like `git-rebase`.

Each exercise consists in a `git` repository which should be cloned, and the exercice must be made inside that repository.
Each exercice has a "STATEMENT.txt" file stating a situation to solve, a few example files on which to operate with (mostly) git commands, and a "SOLUTION.txt" explaining how to solve the problem if you can't solve the exercise.

If you break the repository when trying to solve the exercice, don't worry! Just delete the repo and clone it again to start over.

* [Amend exercise](https://framagit.org/git-exercises/amend-exercise)
* [Rebase's edit mode exercise](https://framagit.org/git-exercises/rebase-edit-exercise)
* [Rebase's fixup mode exercise](https://framagit.org/git-exercises/rebase-fixup-exercise)
* [Handling rebase conflicts exercise](https://framagit.org/git-exercises/rebase-conflict-exercise)
* [Swapping commits exercise](https://framagit.org/git-exercises/swap-commits-exercise)
* [Rebase exec exercise](https://framagit.org/git-exercises/rebase-exec-exercise)
* [Bisect exercise](https://framagit.org/git-exercises/bisect-exercise)
* More exercises to come!
